# Team Members

Steven Thompson

Kyle Ungersma

Spencer DeBuf

Provide three ideas for a tutorial. For each idea write a paragraph that address the following.
#1. Provide a working title for the tutorial.
#2. Provide an objective. That is, what do you hope the reader will learn by working through your tutorial.
#3. Provide a description of the value.
That is, why should someone spend their time reading from your tutorial

# Idea 1
Creating Functional Dependencies
The objective is to explain function dependencies and their importance in databases. It is important to understand 
these constraints when building a database. These dependencies can have a great impact on how the data can be modeled.

# Idea 2
Joins in SQL
Joins in SQL are a simple yet powerful tool to be used. There are many different ways to use joins and it is important to know which is the right one for what you are doing.

# Idea 3
Foreign Key handling 
Foreign keys can be a frustrating topic to deal with if you do not have a clear understanding of them. They can lead to frustration down the road when you can no figure out why something cant be deleted. 

# Idea 4
ER models and different normal forms
ER models are important for modeling different ideas for database structures. They take some time to understand all the symbols and their different forms. However, once understood they become a crucial tool be used. 